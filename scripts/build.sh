#!/bin/bash

export LC_ALL=C
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.
FILES="${BITBUCKET_CLONE_DIR}${SOURCE_PATH}"
INCLUDES="${BITBUCKET_CLONE_DIR}${INCLUDE_TOTVS};${BITBUCKET_CLONE_DIR}${INCLUDE_CUSTOM}"
OUTREPORT="/protheus12/apo/"
FILE_ERROR="${OUTREPORT}"compile_errors.log
ERROR=0

echo
echo "Files    -> ${FILES}"
echo "Includes -> ${INCLUDES}"
echo

# Cria diretórios para os RPO.
mkdir /protheus12/apo/custom/ -p
mkdir /protheus12/apo/standard/ -p

# Compila o projeto.
cd /protheus12/bin/appserver/
./appsrvlinux -compile -env=P12 -files=${FILES} -includes=${INCLUDES} -outreport="${OUTREPORT}" || ERROR=$?

# Verifica se a compilação ocorreu com sucesso.
if [[ $ERROR -eq 0 && -f "${FILE_ERROR}" && ! -s "${FILE_ERROR}" ]]; then
	echo "******************************"
	echo "*   RPO built successfully   *"
	echo "******************************"
else
	if [[ $ERROR -eq 0 ]]; then
		$ERROR=1
	fi
	if [[ -f "${FILE_ERROR}" && -s "${FILE_ERROR}" ]]; then
		cat "${FILE_ERROR}"
	fi
	echo "RPO ** NOT ** compiled properly - error $ERROR"
	exit $ERROR
fi

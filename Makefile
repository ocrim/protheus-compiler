.PHONY: fresh build release
.DEFAULT_GOAL := fresh

fresh: build clean

build:
	# Monta build 19 (Lobo-guará).
	cp -r ./scripts/ ./build-19/
	$(MAKE) -C ./build-19/ build
	
	# Monta build 20 (Harpia).
	cp -r ./scripts/ ./build-20/
	$(MAKE) -C ./build-20/ build

release: build
	$(MAKE) -C ./build-19/ release
	$(MAKE) -C ./build-20/ release_latest

clean:
	# Clears the environment.
	rm -r ./build-19/scripts
	rm -r ./build-20/scripts

